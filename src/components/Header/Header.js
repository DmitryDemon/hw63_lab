import React from 'react';
import {NavLink} from 'react-router-dom';

import './Header.css';

const Header = () => {
    return (
        <div>
            <div className='header'>
                <h1 className='myBlog'>My Blog</h1>
                <div className='itemWrapper'>
                    <NavLink className='item' to='/'>Home</NavLink>
                    <NavLink className='item' to='/posts/add' >Add</NavLink>
                    <NavLink className='item' to='/about'>About</NavLink>
                    <NavLink className='item last' to='/contacts' >Contacts</NavLink>
                </div>
            </div>
        </div>
    );
};

export default Header;