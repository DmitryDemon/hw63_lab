import React, {Component} from 'react';
import axios from '../../../../axios-post';

import Button from "../../../UI/Button/Button";

import './GetOnePostInDesctop.css';

class GetOnePostInDesctop extends Component {

    state={
        post: {},
    };

    componentDidMount() {
        console.log(this.props);
        axios.get(this.props.match.url + '.json').then(response => {
            console.log(response);
            this.setState({post: response.data})
        }).catch(error => {
            console.log(error);
        });
    }

    handleRemove = () => {
        axios.delete(this.props.match.url + '.json')
            .then(res => {
               this.props.history.push('/');
            })
            .catch((err) => {
                console.log(err);
            })
    };

    handlerEdit = () => {
        const id = this.props.match.params.id;
        this.props.history.push(`/post/${id}/edit`)
    };

    render() {
        return (
            <div  className="PostOne" >
                <h3>{this.state.post.title}</h3>
                <p className="PostDate">{this.state.post.date}</p>
                <p className="PostDesc">{this.state.post.description}</p>
              <Button click={()=>this.handleRemove()} btnType="Danger">Delete</Button>
              <Button click={this.handlerEdit} btnType="Primary">Edit</Button>
            </div>
        );
    }
}

export default GetOnePostInDesctop;