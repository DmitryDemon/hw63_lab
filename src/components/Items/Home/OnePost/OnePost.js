import React from 'react';
import Button from "../../../UI/Button/Button";

import './OnePost.css';

const OnePost = (props) => {
    return (
        <div className='onePost'>
            <p className='data'>Create on : {props.data}</p>
            <p className='title'>{props.title}</p>
            <Button click={props.clickHandler} btnType='Success'>Read more >></Button>
        </div>
    );
};

export default OnePost;