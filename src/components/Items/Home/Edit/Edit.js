import React, {Component} from 'react';
import axios from "../../../../axios-post";

import FormPost from "../../Add/FormPost/FormPost";


class Edit extends Component {

    state={
        title: '',
        description: '',
        date: ''
    };

    getPostUrl = () => {
      const id = this.props.match.params.id;
      return 'post/' + id + '.json';
    };

  valueChanged = event => {
    const {name, value} = event.target;
    this.setState({[name]: value});
  };

    componentDidMount(){
        axios.get(this.getPostUrl()).then(response => {
            this.setState({title:response.data.title,
                          description: response.data.description,
                          date: response.data.date})
        })
    };

    editPost = event => {
      event.preventDefault();
      axios.put(this.getPostUrl(), this.state).then(() => {
          this.props.history.push('/')
      });
    };

    render() {
      console.log(this.props.match.params.id);
      let form = <FormPost
        submited={this.editPost}
        title={this.state.title}
        description={this.state.description}
        changed={this.valueChanged}/>;
      if (!this.state.title) {
          form = <div>Loading...</div>
      }
      return (
            <div>
              {form}
            </div>
        );
    }
}

export default Edit;