import React, {Component} from 'react';
import axios from '../../../axios-post';

import Header from '../../Header/Header';
import OnePost from "./OnePost/OnePost";

import './Home.css';

class Home extends Component {

  state = {
    posts: {}
  };

  componentDidMount() {
    axios.get('post.json').then(response => {
        this.setState({posts: response.data})
    }).catch(error => {
      console.log(error);
    });
  }

  readMoreHandler = (id) => {
    this.props.history.push(`/post/${id}`)
  };

  render() {
      console.log(this.props.history);
      const allPosts = () => {
      return Object.keys(this.state.posts).map((key, id) => {
        return <OnePost
          clickHandler={() => this.readMoreHandler(key)}
          key={key}
          title={this.state.posts[key].title}
          data={this.state.posts[key].date}/>
      })
      };

    return (
      <div className="Home">
        <Header/>
          {allPosts().reverse()}
      </div>
    );
  }
}

export default Home;