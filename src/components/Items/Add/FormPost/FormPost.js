import React from 'react';

import Button from "../../../UI/Button/Button";

import './FormPost.css';

const FormPost = props =>  {
    return (
      <div className="FormPost">
        <h3>Add new post</h3>
        <form onSubmit={props.submited}>
          <label htmlFor="title">Title</label>
          <input className="title" type="text" name="title" id="title" placeholder="Enter title"
                 value={props.title} onChange={props.changed}
          />
          <label htmlFor="description">Description</label>
          <textarea className="description" id="description" name="description"
                    value={props.description} onChange={props.changed}
          />
          <Button btnType="Primary">Save</Button>
        </form>
      </div>
    );
  };


export default FormPost;