import React, {Component} from 'react';
import axios from "../../../axios-post";

import Header from '../../Header/Header';
import FormPost from "./FormPost/FormPost";

import './Add.css';

class Add extends Component {

  constructor(props) {
    super(props);
    if (props.post) {
      this.state = {...props.post}
    } else {
      this.state = {
        title: '',
        description: ''
      };
    }
  }

  valueChanged = event => {
    const {name, value} = event.target;
    this.setState({[name]: value});
  };

  postHandler = event => {
    event.preventDefault();

    const post = {
      title: this.state.title,
      description: this.state.description,
      date: new Date()
    };

    axios.post('post.json', post).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <div>
        <Header/>
        <FormPost submited={this.postHandler} changed={this.valueChanged}/>
      </div>
    );
  }
}

export default Add;