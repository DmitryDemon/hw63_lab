import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import Home from "./components/Items/Home/Home";
import Add from "./components/Items/Add/Add";
import About from "./components/Items/About/About";
import Contacts from "./components/Items/Contacts/Contacts";
import GetOnePostInDesctop from "./components/Items/Home/GetOnePostInDesctop/GetOnePostInDesctop";
import Edit from "./components/Items/Home/Edit/Edit";

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <BrowserRouter>
              <Switch>
                  <Route path="/" exact component={Home}/>
                  <Route path="/posts/add" component={Add}/>
                  <Route path="/about" component={About}/>
                  <Route path="/contacts" component={Contacts}/>
                  <Route exact path="/post/:id/edit/" component={Edit}/>
                  <Route path="/post/:id" component={GetOnePostInDesctop}/>
              </Switch>
          </BrowserRouter>
      </div>
    );
  }
}

export default App;
