import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://hw63labdimasanya.firebaseio.com/'
});

export default instance;